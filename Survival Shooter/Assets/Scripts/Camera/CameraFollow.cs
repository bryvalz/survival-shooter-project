﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{    
    public Transform target;
    public Vector3 offset;
    public float smoothing = 5f;

    private void LateUpdate()
    {
        //Menapatkan posisi untuk camera
        Vector3 targetCamPos = target.position + offset;

        //set posisi camera dengan smoothing
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
