﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;

    [SerializeField] MonoBehaviour factory;
    public float bunnySpawnTime = 3f;
    public float bearSpawnTime = 5f;
    public float elephantSpawnTime = 7f;

    IFactory Factory { get { return factory as IFactory; } }

    private void Update()
    {
        //hitung waktu delay spawn
        bunnySpawnTime -= Time.deltaTime;
        bearSpawnTime -= Time.deltaTime;
        elephantSpawnTime -= Time.deltaTime;

        //Jika player telah mati maka tidak membuat enemy baru
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        //Mendapatkan nilai random
        int spawnPointIndex = Random.Range(0, 3);

        //Spawn Enemy
        //Bunny
        if (bunnySpawnTime <= 0)
        {
            Factory.FactoryMethod(0, spawnPointIndex);
            bunnySpawnTime = 3f;
        }

        //Bear
        if (bearSpawnTime <= 0)
        {
            Factory.FactoryMethod(1, spawnPointIndex);
            bearSpawnTime = 5f;
        }

        //Elephant
        if (elephantSpawnTime <= 0)
        {
            Factory.FactoryMethod(2, spawnPointIndex);
            elephantSpawnTime = 7f;
        }
    }
}
